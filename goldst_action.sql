-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 13 2018 г., 15:33
-- Версия сервера: 5.6.38
-- Версия PHP: 5.5.38

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `goldst_action`
--

-- --------------------------------------------------------

--
-- Структура таблицы `kalin_nefrit`
--

CREATE TABLE `kalin_nefrit` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `surname` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `time` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `articul` char(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `kalin_nefrit`
--

INSERT INTO `kalin_nefrit` (`id`, `name`, `surname`, `phone`, `time`, `email`, `articul`) VALUES
(3, 'Арнольд', 'Иванов', '+7(988) 833-8389', '14:36:04 13-06-18', 'gem@mail.ru', '8833893'),
(4, 'Джозеф', 'Юркен', '+7(893) 203-2230', '14:39:12 13-06-18', 'dk@msila.ru', '9030392');

-- --------------------------------------------------------

--
-- Структура таблицы `kalin_rubin`
--

CREATE TABLE `kalin_rubin` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `surname` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `time` varchar(255) NOT NULL,
  `email` varchar(100) NOT NULL,
  `articul` char(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `kalin_rubin`
--

INSERT INTO `kalin_rubin` (`id`, `name`, `surname`, `phone`, `time`, `email`, `articul`) VALUES
(1, 'Арториус', 'Кастус', '+7(898) 288-2828', '11:13:18 13-06-18', 'f@a.ru', '9999999');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `kalin_nefrit`
--
ALTER TABLE `kalin_nefrit`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `kalin_rubin`
--
ALTER TABLE `kalin_rubin`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `kalin_nefrit`
--
ALTER TABLE `kalin_nefrit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT для таблицы `kalin_rubin`
--
ALTER TABLE `kalin_rubin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
