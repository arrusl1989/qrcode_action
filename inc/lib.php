<?php

/**
 * Очищаем строку
 * @param  object $connection идентификатор соединения с БД
 * @param  string $str строка
 * @return string
 */
function clearStr($connection, $str) {
    $str = trim(strip_tags($str));
    return mysqli_real_escape_string($connection, $str);
}

/**
 * Очищаем число, может быть только целым положительным числом
 * @param  int  число
 * @return int получаем либо 0, либо целое положительное число
 */
function clearInt($int) {
    return abs((int)$int);
}

/**
 * Добавление записи в базу после заполнения формы
 * @param  object $connection идентификатор соединения с БД
 * @param  string $name
 * @param  string $surname
 * @param  string $phone
 * @param  string $tableName
 * @param  string $time
 * @param  string $email
 * @return bool
 */
function addInfoToBase($connection, $name, $surname, $phone, $tableName, $time, $email) {
    $sql = "INSERT INTO $tableName (name,surname,phone,time,email) VALUES(?,?,?,?,?)";

    if (!$stmt = mysqli_prepare($connection, $sql))
        return false;

    mysqli_stmt_bind_param($stmt, "sssss", $name, $surname, $phone, $time, $email);

    mysqli_stmt_execute($stmt);

    mysqli_stmt_close($stmt);

    return true;
}

/**
 * Добавление нового события
 * @param  object $connection  идентификатор соединения с БД
 * @param  string $symbol_id   символьный идентификатор события
 * @param  string $title       название события
 * @return bool
 */
function addEvent($connection, $symbol_id, $title) {
    $sql = "INSERT INTO _events (`symbol_id`, `title`) VALUES(?,?)";

    if (!$stmt = mysqli_prepare($connection, $sql))
        return false;

    mysqli_stmt_bind_param($stmt, "ss", $symbol_id, $title);

    mysqli_stmt_execute($stmt);

    mysqli_stmt_close($stmt);

    return true;
}

/**
 * Обновление события
 * @param  object $connection  идентификатор соединения с БД
 * @param  int    $id          идентификатор
 * @param  string $title       название события
 * @return bool
 */
function updateEvent($connection, $id, $title) {
    $sql = "UPDATE `_events` SET `title` = ? WHERE `_events`.`id` = ?";

    if (!$stmt = mysqli_prepare($connection, $sql))
    return false;

    mysqli_stmt_bind_param($stmt, "si", $title, $id);

    mysqli_stmt_execute($stmt);

    mysqli_stmt_close($stmt);

    return true;
}

/**
 * Удаление события
 * @param  object $connection  идентификатор соединения с БД
 * @param  int    $id          идентификатор
 * @return bool
 */
function delEvent($connection, $id) {
    $sql = "DELETE FROM `_events` WHERE `_events`.`id` = $id";

    if (!$stmt = mysqli_prepare($connection, $sql))
        return false;

    mysqli_stmt_execute($stmt);

    mysqli_stmt_close($stmt);

    return true;
}

/**
 * Получить событие по идентификатору
 * @param  object $connection  идентификатор соединения с БД
 * @param  int    $id          идентификатор
 * @return bool
 */
function getEventById($connection, $id) {
    $sql = "SELECT * FROM _events WHERE id = $id";

    if(!$result = mysqli_query($connection, $sql))
        return false;

    $event = mysqli_fetch_all($result, MYSQLI_ASSOC);

    mysqli_free_result($result);

    return $event;
}

/**
 * Создание таблицы в БД
 * @param  object $connection идентификатор соединения с БД
 * @param  string $tableName
 * @return bool
 */
function createTable($connection, $tableName) {
    $sql = "CREATE TABLE $tableName (
        `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
        `name` varchar(100) NOT NULL,
        `surname` varchar(100) NOT NULL,
        `phone` varchar(100) NOT NULL,
        `time` varchar(255) NOT NULL,
        `email` varchar(100) NOT NULL,
        PRIMARY KEY(`id`)
      ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

    if (!$stmt = mysqli_prepare($connection, $sql))
        return false;

    mysqli_stmt_execute($stmt);

    mysqli_stmt_close($stmt);

    return true;
}


/**
 * Удаление таблицы в БД
 * @param  object $connection идентификатор соединения с БД
 * @param  string $tableName
 * @return bool
 */
function dropTable($connection, $name) {
    $sql = "DROP TABLE `$name`";

    if (!$stmt = mysqli_prepare($connection, $sql))
        return false;

    mysqli_stmt_execute($stmt);

    mysqli_stmt_close($stmt);

    return true;
}

/**
 * Получить все данные из таблицы
 * @param  object $connection идентификатор соединения с БД
 * @param  string $tableName
 * @param  string $sort       название поля сортировки
 * @param  bool   $assoc      вид получаемого массива ASSOC или NUM
 * @return array
 */
function getAllDataFromTable($connection, $tableName, $assoc = true, $sort="id") {
    $sql = "SELECT * FROM $tableName ORDER BY $sort";


    if (!$result = mysqli_query($connection, $sql))
    return false;

    $arrayType = $assoc ? MYSQLI_ASSOC : MYSQLI_NUM;

    $data = mysqli_fetch_all($result, $arrayType);
    mysqli_free_result($result);
    return $data;
}

/**
 * Формирование массива вида $arr = [$field => $field2]
 * @param  array  $arr     исходный массив
 * @param  string $field
 * @param  string $field2
 * @return array
 */
function getDesiredVal($arr, $field, $field2) {
    $arr2 = [];
    foreach ($arr as $key) {
        $arr2[$key[$field]] = $key[$field2] ;
    }
    return $arr2;
}

/**
 * Создает CSV файл из переданных в массиве данных.
 * @param  array  $create_data  Массив данных из которых нужно созать CSV файл.
 * @param  string $file         Путь до файла 'path/to/test.csv'. Если не указать, то просто вернет результат.
 * @return string/false        CSV строку или false, если не удалось создать файл.
 *
 */
function createCsvFile($create_data, $file = null, $col_delimiter = ';', $row_delimiter = "\r\n") {

    if( ! is_array($create_data) )
    return false;

	if( $file && ! is_dir( dirname($file) ) )
        return false;

	// строка, которая будет записана в csv файл
	$CSV_str = '';

	// перебираем все данные
	foreach( $create_data as $row ){
		$cols = array();

		foreach( $row as $col_val ){
			// строки должны быть в кавычках ""
			// кавычки " внутри строк нужно предварить такой же кавычкой "
			if( $col_val && preg_match('/[",;\r\n]/', $col_val) ){
				// поправим перенос строки
				if( $row_delimiter === "\r\n" ){
					$col_val = str_replace( "\r\n", '\n', $col_val );
					$col_val = str_replace( "\r", '', $col_val );
				}
				elseif( $row_delimiter === "\n" ){
					$col_val = str_replace( "\n", '\r', $col_val );
					$col_val = str_replace( "\r\r", '\r', $col_val );
				}

				$col_val = str_replace( '"', '""', $col_val ); // предваряем "
				$col_val = '"'. $col_val .'"'; // обрамляем в "
			}

			$cols[] = $col_val; // добавляем колонку в данные
		}

		$CSV_str .= implode( $col_delimiter, $cols ) . $row_delimiter; // добавляем строку в данные
	}

	$CSV_str = rtrim( $CSV_str, $row_delimiter );

	// задаем кодировку windows-1251 для строки
	if( $file ){
		$CSV_str = iconv( "UTF-8", "cp1251",  $CSV_str );

		// создаем csv файл и записываем в него строку
		$done = file_put_contents( $file, $CSV_str );

		return $done ? $CSV_str : false;
	}

	return $CSV_str;

}

/**
 * Формирование данных для CSV
 * @param  object $connection идентификатор соединения с БД
 * @param  string $symbol_id      название таблицы
 * @return array
 */
function getDataForCsv($connection, $symbol_id) {
    $table = getAllDataFromTable($connection, $symbol_id, false);
    $columns = getColumnsName($connection, $symbol_id);

    $csv = [];
    foreach ($columns as $key => $value) {
        foreach($value as $key2 => $value2) {
            $csv[] = $value2;
        }
    }
    array_unshift($table, $csv);

    return $table;
}


/**
 * Получить названия колонок таблицы
 * @param  object $connection идентификатор соединения с БД
 * @param  string $tableName
 * @return array
 */
function getColumnsName($connection, $tableName) {
    $sql = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = 'qrcode-action' AND TABLE_NAME = '$tableName'";

    if(!$result = mysqli_query($connection, $sql))
        return false;
    $columnsName = mysqli_fetch_all($result, MYSQLI_NUM);
    mysqli_free_result($result);
    return $columnsName;
}

/**
 * Получить идентификатор последней записи
 * @param  object $connection идентификатор соединения с БД
 * @param  string $tableName
 * @return int
 */
function getLastRecordId($connection, $tableName) {
    $sql = "SELECT id FROM $tableName ORDER BY id DESC limit 1";
    if(!$result = mysqli_query($connection, $sql))
        return false;
    $number = mysqli_fetch_all($result, MYSQLI_NUM);
    mysqli_free_result($result);
    return $number[0][0];
}

function adopt($text) {
    return '=?UTF-8?B?'.Base64_encode($text).'?=';
}

/**
 * Получаем путь к представлению
 * @param string $name        имя файла (без .php)
 * @param string $path        имя подпапки файла (без последнего /)
 * @param string $base_path   путь к папке файла (без последнего /)
 * @return string/false $file_path
 */
function getView($name, $path,  $base_path = '/views')
{

  //удаляем лишнее из имени файла
  $base_name = str_replace('.php', '', $name);

  if (!empty($base_name)) {
    //удаляем последний слеш в подпапке
    $path = preg_replace('#/$#', '', $path);
    //удаляем последний слеш в папке
    $base_path = preg_replace('#/$#', '', $base_path);

    $file_path = dirname(__DIR__) . $base_path . '/' . $path . '/' . $base_name . '.php';

    if (!file_exists($file_path)) {
      return false;
    }
  }

  return $file_path;
}


/**
 * Отправка письма
 * @param  string $email        почтовый адрес клиента
 * @param  string $admin_email  почтовый адрес админа
 * @param  string $name         имя клиента
 * @param  int    $number       номер участника
 * @return bool
 */
function sendMail($email, $admin_email, $name, $number) {
    $email .= ', ' . $admin_email;
    $form_subject = 'Регистрация в конкурсе';

    $message = $message = <<<HEREDOC
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"><html xmlns="http://www.w3.org/1999/xhtml" lang="ru" xml:lang="ru"><head><link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800&amp;subset=cyrillic" rel="stylesheet"><link href="https://fonts.googleapis.com/css?family=Philosopher&amp;subset=cyrillic" rel="stylesheet"><meta http-equiv="Content-Type" content="text/html; charset=utf-8"><meta name="viewport" content="width=device-width"><style>@media only screen{html{min-height:100%;background:#fff}}@media only screen and (max-width:600px){table.body img{width:auto;height:auto}table.body center{min-width:0!important}table.body .container{width:95%!important}table.body .columns{height:auto!important;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;box-sizing:border-box;padding-left:0!important;padding-right:0!important}th.small-12{display:inline-block!important;width:100%!important}}</style></head><body style="-moz-box-sizing:border-box;-ms-text-size-adjust:100%;-webkit-box-sizing:border-box;-webkit-text-size-adjust:100%;Margin:0;box-sizing:border-box;color:#444341;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:17px;font-weight:400;line-height:1.3;margin:0;min-width:100%;padding:0;text-align:left;width:100%!important"><table class="body" style="Margin:0;background:#fff;border-collapse:collapse;border-spacing:0;color:#444341;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:17px;font-weight:400;height:100%;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><td class="center" align="center" valign="top" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#444341;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:17px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word"><center data-parsed="" style="min-width:600px;width:100%"><!--logo--><table align="center" class="container float-center" style="Margin:0 auto;background:#fff;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:600px"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#444341;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:17px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word"><table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="50px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#444341;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:50px;font-weight:400;hyphens:auto;line-height:50px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table><table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#444341;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:17px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:5px;padding-left:0;padding-right:0;text-align:left;width:600px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#444341;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:17px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><a href="https://dengi.modimio.ru/" style="Margin:0;color:#8f3537;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;text-decoration:none"><center data-parsed="" style="min-width:600px;width:100%"><img src="https://dengi.modimio.ru/assets/8d031130/img/logo.png" alt="GoldStream" align="center" class="float-center" style="-ms-interpolation-mode:bicubic;Margin:0 auto;border:none;clear:both;display:block;float:none;margin:0 auto;max-width:100%;outline:0;text-align:center;text-decoration:none;width:auto"></center></a></th><th class="expander" style="Margin:0;color:#444341;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:17px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th></tr></table></th></tr></tbody></table></td></tr></tbody></table><table align="center" class="container float-center" style="Margin:0 auto;background:#fff;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:600px"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#444341;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:17px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word"><table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#444341;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:17px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:5px;padding-left:0;padding-right:0;text-align:left;width:600px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#444341;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:17px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><center data-parsed="" style="min-width:600px;width:100%"><h1 class="title bold text-center float-center" align="center" style="Margin:0;Margin-bottom:10px;color:inherit;font-family:Philosopher,sans-serif;font-size:40px;font-weight:700;line-height:1.3;margin:0;margin-bottom:10px;padding:20px 0 20px 0;text-align:center;word-wrap:normal">$name, здравствуйте!</h1><table class="spacer float-center" style="Margin:0 auto;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="20px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#444341;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:20px;font-weight:400;hyphens:auto;line-height:20px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table><p class="txt float-center" align="center" style="Margin:0;Margin-bottom:0!important;color:#444341;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:17px;font-weight:400;line-height:25px;margin:0;margin-bottom:0!important;padding:0 35px 0 18px;text-align:left">Поздравляем, вы успешно прошли регистрацию.</p><table class="spacer float-center" style="Margin:0 auto;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="20px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#444341;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:20px;font-weight:400;hyphens:auto;line-height:20px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table><p class="txt bold float-center" align="center" style="Margin:0;Margin-bottom:0!important;color:#444341;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:17px;font-weight:700;line-height:25px;margin:0;margin-bottom:0!important;padding:0 35px 0 18px;text-align:left">Ваш регистрационный номер: $number</p><table class="spacer float-center" style="Margin:0 auto;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="20px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#444341;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:20px;font-weight:400;hyphens:auto;line-height:20px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table><p class="txt float-center" align="center" style="Margin:0;Margin-bottom:0!important;color:#444341;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:17px;font-weight:400;line-height:25px;margin:0;margin-bottom:0!important;padding:0 35px 0 18px;text-align:left">Чтобы участвовать в розыгрыше вы также должны быть подписчиком нашего журнала “Деньги Мира”. Если вы еще не подписались, сделайте это прямо сейчас на нашем сайте <a href="https://dengi.modimio.ru/" style="Margin:0;color:#8f3537;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;text-decoration:none">https://dengi.modimio.ru/</a></p><table class="spacer float-center" style="Margin:0 auto;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="10px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#444341;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:10px;font-weight:400;hyphens:auto;line-height:10px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table><p class="txt float-center" align="center" style="Margin:0;Margin-bottom:0!important;color:#444341;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:17px;font-weight:400;line-height:25px;margin:0;margin-bottom:0!important;padding:0 35px 0 18px;text-align:left">Информацию о разыгрываемых призах и условиях розыгрыша вы всегда можете узнать по адресу <a href="https://dengi.modimio.ru/prizeinfo" style="Margin:0;color:#8f3537;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;text-decoration:none">https://dengi.modimio.ru/prizeinfo</a></p><table class="spacer float-center" style="Margin:0 auto;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="40px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#444341;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:40px;font-weight:400;hyphens:auto;line-height:40px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table></center></th><th class="expander" style="Margin:0;color:#444341;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:17px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th></tr></table></th></tr></tbody></table></td></tr></tbody></table><!--logo--><table align="center" class="container float-center" style="Margin:0 auto;background:#fff;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:600px"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#444341;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:17px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word"><table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#444341;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:17px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:5px;padding-left:0;padding-right:0;text-align:left;width:600px"><table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%"><tr style="padding:0;text-align:left;vertical-align:top"><th style="Margin:0;color:#444341;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:17px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><p class="text-center black" style="Margin:0;Margin-bottom:10px;color:#282828;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:17px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:center">телефон горячей линии</p><center data-parsed="" style="min-width:600px;width:100%"><a href="tel:88005054383" class="font22-8 fontLight phone float-center" align="center" style="Margin:0;color:#282828!important;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:22.8px;font-weight:300;line-height:1.3;margin:0;padding:0;text-align:center!important;text-decoration:none">8 800 505 43 83</a></center></th><th class="expander" style="Margin:0;color:#444341;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:17px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th></tr></table></th></tr></tbody></table></td></tr></tbody></table><table class="spacer float-center" style="Margin:0 auto;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:100%"><tbody><tr style="padding:0;text-align:left;vertical-align:top"><td height="50px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#444341;font-family:'Open Sans',Arial,Helvetica,sans-serif;font-size:50px;font-weight:400;hyphens:auto;line-height:50px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td></tr></tbody></table></center></td></tr></table><!-- prevent Gmail on iOS font size manipulation --><div style="display:none;white-space:nowrap;font:15px courier;line-height:0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div></body></html>
HEREDOC;

    $headers = "MIME-Version: 1.0" . PHP_EOL .
    "Content-Type: text/html; charset=utf-8" . PHP_EOL .
    'From: '.adopt($project_name).' <'.$admin_email.'>' . PHP_EOL .
    'Reply-To: '.$admin_email.'' . PHP_EOL;

    if (mail($email, adopt($form_subject), $message, $headers))
        return true;
}
