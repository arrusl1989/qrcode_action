<?php

/**
 * Данные для подключения к БД
 * @var array
 */
$db = [
    'server'   => 'localhost',
    'username' => 'root',
    'password' => '',
    'name'     => 'qrcode-action'
];


$connection = mysqli_connect(
    $db['server'],
    $db['username'],
    $db['password'],
    $db['name']
);


if (!$connection) {
    echo 'Ошибка: '
    . mysqli_connect_errno()
    . ': '
    . mysqli_connect_error();
      exit();
}

require __DIR__ . '/lib.php';


/**
 * Версия стилей и скриптов
 * @var string
 */
$version = '0.0.1';


/**
 * Почта, которая будет указана как отправитель и на которую будут приходить копии писем-заявок
 * @var string
 */
$admin_email = 'arrusl@yandex.ru';


/**
 * Ссылки на социальные сети
 * @var string
 */
$vk = 'https://vk.com/dengi.modimio';
$insta = 'https://www.instagram.com/dengi.modimio/';


/**
 * Title
 * @var string
 */
$title = 'Деньги Мира - Modimio Collections';

