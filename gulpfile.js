let
		gulp           = require('gulp'),
		scss           = require('gulp-sass'),
		browsersync    = require('browser-sync').create(),
		concat         = require('gulp-concat'),
		uglify         = require('gulp-uglify'),
		cleanCSS       = require('gulp-clean-css'),
		rename         = require('gulp-rename'),
		del            = require('del'),
		// cache          = require('gulp-cache'),
		autoprefixer   = require('gulp-autoprefixer'),
		// ftp            = require('vinyl-ftp'),
		notify         = require('gulp-notify'),
		gcmq 		   = require('gulp-group-css-media-queries');





/*============================
*       Build in dist
============================*/

function buildBase(){
	return gulp.src([
        '!admin/**/*', '**/*.php'
        ]).pipe(gulp.dest('dist'));
}

function buildCss(){
	return gulp.src([
		'css/**/*.css',
		])
		.pipe(cleanCSS({level: 2}))	
		.pipe(gulp.dest('dist/css'));
}

function buildFonts(){
	return gulp.src([
		'fonts/**/*',
		]).pipe(gulp.dest('dist/fonts'));
}

function buildJs(){
	return gulp.src([
		'js/scripts.min.js',
		]).pipe(gulp.dest('dist/js'));
}

function buildAdmin() {
    return gulp.src([
        'admin/**/*.*',
        ]).pipe(gulp.dest('dist/admin/'));
}

function buildHtaccess() {
    return gulp.src([
        '.htaccess',
        ]).pipe(gulp.dest('dist/'));
}

function buildImg() {
    return gulp.src([
        'img/**/*',
        ]).pipe(gulp.dest('dist/img'));
} 





/*============================
*       Functions
============================*/

// BrowserSync
function browserSync(done) {
    browsersync.init({
        open: false,
		proxy: "http://qrcode-action:80",
		notify: false,
        port: 3000
		// tunnel: true,
		// tunnel: "goldstream", //Demonstration page: http://goldstream.localtunnel.me
    });
    done();
}

// BrowserSync Reload
function browserSyncReload(done) {
    browsersync.reload();
    done();
}

function scripts() {
		return gulp.src([
                'libs/jquery/dist/jquery.min.js',
				'libs/jquery.maskedinput/jquery.maskedinput.min.js',
				'js/_source/*.js' // Всегда в конце
				])
		.pipe(concat('scripts.min.js'))
		.pipe(uglify()) // Минимизировать весь js (на выбор)
		.pipe(gulp.dest('js/'))
		.pipe(browsersync.stream());
};

function styles() {
		return gulp.src('scss/**/*.scss')
		.pipe(scss({outputStyle: 'expand'}).on("error", notify.onError()))
		.pipe(rename({suffix: '.min', prefix : ''}))
		.pipe(autoprefixer({ grid: true, browsers: ['>1%'] }))
		.pipe(gcmq()) // для финальной сборки, закомментировать при разработке
		//.pipe(gcmq()) // для разработки, закомментировать при финальной сборке
		.pipe(gulp.dest('css/'))
		.pipe(browsersync.stream());
};

function removedist() {
	return del('dist');
};

function watchFiles() {
		gulp.watch('scss/**/*', styles);
		gulp.watch('js/_source/*.js', scripts);
		gulp.watch('**/*.php', browserSyncReload);
}





/*============================
*    Define complex tasks
============================*/

const build =
	gulp.series(removedist, styles, scripts,
		gulp.parallel(buildImg, buildBase, buildAdmin, buildFonts, buildCss, buildJs));

const watch = gulp.series(styles, scripts, gulp.parallel(watchFiles, browserSync));





/*============================
*    Export tasks
============================*/

exports.removedist = removedist;
// Финальная сборка
exports.build = build;
exports.watch = watch;

exports.default = watch;
