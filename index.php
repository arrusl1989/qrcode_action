<?php
	require_once __DIR__ . '/inc/config.php';
	$event = $_GET['event'] ? $_GET['event'] : 'gift';

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {

		$name = clearStr($connection, $_POST['name']);
		$surname = clearStr($connection, $_POST['surname']);
		$phone = clearStr($connection, $_POST["phone"]);
		$email = clearStr($connection, $_POST['email']);
		$time = strftime('%H:%M:%S %d-%m-%y');
		$tableName = clearStr($connection, $_POST['event']);

		if(!addInfoToBase($connection, $name, $surname, $phone, $tableName, $time, $email)){
			return false;
		}

		$number = getLastRecordId($connection, $tableName);

		if (!sendMail($email, $admin_email, $name, $number) ){
			return false;
		}
			
		$result = [
			'status' => 'done'
		];

		echo json_encode($result);
		exit;
	}

	include getView('v-index', 'templates');