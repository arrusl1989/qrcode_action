<!DOCTYPE html>
<html lang="ru">

  <head>

    <meta charset="utf-8">

    <title><?= $title; ?></title>
    <meta name="description" content="Регистрация в розыгрыше “ДЕНЬГИ МИРА”">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <meta property="og:image" content="path/to/image.jpg">
    <link rel="apple-touch-icon" sizes="57x57" href="img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <style>
      body {
        opacity: 0;
        height: 100vh;
        overflow-x: hidden;
      }

      html {
        background-color: #fff;
      }
    </style>


  </head>

  <body>

    <div class="container">
      <main class="page">

        <img class="logo" src="img/logo.png" alt="деньги мира" width="130" height="60">

        <h1 class="title">Регистрация в розыгрыше <br> “ДЕНЬГИ МИРА”</h1>

        <form class="form js-ajax-form" action="<?= $_SERVER["REQUEST_URI"]; ?>" method="POST" >

          <div class="required">
            <input class="form__input" type="text" name="name" placeholder="Имя">
          </div>

          <div class="required">
            <input class="form__input" type="text" name="surname" placeholder="Фамилия">
          </div>

          <div class="required">
            <input class="form__input" type="tel"  name="phone" placeholder="+7 (___) ___-__-__">
          </div>

          <div class="required">
            <input class="form__input" type="email" name="email" placeholder="Электронная почта">
          </div>

          <input type="hidden" name="event" value="<?= $event; ?>">

          <p class="form__txt">Нажимая кнопку «Отправить», я подтверждаю, что даю своё согласие на обработку моих персональных данных в соответствии с Федеральным законом №152 от 27.07.2006 года «О персональных данных» на условиях и для целей, определённых в Согласии на обработку персональных данных</p>

          <button type="submit" class="btn btn_middle btn_center">отправить</button>

        </form>
      </main>
    </div>

    <div class="modal" id="done">

      <div class="container container_done">

        <h2 class="modal__title">ВАШИ ДАННЫЕ<br> УСПЕШНО ОТПРАВЛЕНЫ</h2>

        <p class="modal__txt">В течение дня на электронную почту, которую вы указали при регистрации, придет письмо с подтверждением вашего участия в розыгрыше.</p>

        <img class="modal__img" src="img/done.svg" alt="деньги мира" width="130" height="130">

        <h3 class="modal__subtitle">Присоединяйся</h3>
        <p class="modal__secondary-txt">Следи за розыгрышем в наших<br> соц. сетях</p>

        <div class="social">
          <a class="modal__first-link social__link" target="_blank" href="<?= $insta; ?>">
            <img src="img/instagram.svg" alt="" width="50" height="50">
          </a>
          <a class="social__link" target="_blank" href="<?= $vk; ?>">
            <img src="img/vk.svg" alt="" width="50" height="50">
          </a>
        </div>
      </div>

    </div>

    <div class="modal" id="fail">

      <div class="container container_fail">

        <h2 class="modal__title">ЧТО-ТО ПОШЛО НЕ ТАК</h2>

        <p class="modal__txt">Ваши данные не были отправлены. Проверьте стабильность вашего интернет соединения. Повторите попытку еще раз.</p>

        <img class="modal__img modal__img_fail" src="img/fail.svg" alt="деньги мира" width="130" height="146">


        <button type="button" class="btn btn_middle btn_center js-repeat">повторить</button>

        <h3 class="modal__subtitle modal__subtitle_small">ЕСЛИ ОШИБКА ПОВТОРИТЬСЯ,ЗВОНИТЕ:</h3>

        <a class="modal__phone" href="tel:88005054383">8 800 505 43 83</a>
      </div>

    </div>

    <link rel="stylesheet" href="css/main.min.css?v=<?= $version; ?>">
    <script src="js/scripts.min.js?v=<?= $version; ?>"></script>

  </body>

</html>

