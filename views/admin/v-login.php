<!DOCTYPE HTML>
<html>
  <head>
    <title>Авторизация</title>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="../img/favicon/favicon-16x16.png" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Roboto&amp;subset=cyrillic" rel="stylesheet">
    <link rel="stylesheet" href="../css/style.css">
  </head>
  <body>

    <div class="container">
      <div class="center">
        <img class="logo" src="../../img/logo.png" class="img" width='130' height='60' alt="деньги мира">
        <h1 class="title"><?= $title; ?></h1>
        <form class="form-login" action="<?= $_SERVER['REQUEST_URI']?>" method="post">
          <label for="login">Логин:</label>
          <input type="text" name="login" value="<?= $login?>" class="placeholder" placeholder="имя пользователя">

          <label for="password">Пароль:</label>
          <input type="password" name="pw" class="placeholder" placeholder="пароль...">

          <input type="submit" value="ВОЙТИ">
        </form>
      </div>
    </div>


  </body>
</html>
