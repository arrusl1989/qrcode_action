<div class="container align-center">
  <a class='btn' href='events.php'>Назад</a>
  <form class="form" action="<?= $_SERVER["SCRIPT_NAME"]; ?>" method="POST">
    <h2 class="title center mb2em"><?= $event['symbol_id'] ?></h2>
    <input type="hidden" name="id" value="<?= $id ?>">

    <label for="name">Название события</label>
    <input type="text" name="name" id="name" value="<?= $event['title'] ?>">

    <input type="submit" value="Сохранить">
  </form>
</div>
