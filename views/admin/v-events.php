<div class="container container_1600">

  <a class='btn' href='index.php?logout'>Выйти</a>
  <a class='btn' href='index.php'>Участники</a>
  <a class='btn' href='addEvent.php'>Добавить событие</a>
  <a class='btn' href='secure/createUser.php'>Добавить пользователя</a>

  <h1 class="title center mb2em">События</h1>

  <table class="simple-little-table" border="1" cellpadding="5" cellspacing="0" width="100%">
    <tr>
      <th>ID</th>
      <th>Символьный ID</th>
      <th>Название события</th>
      <th>Редактировать</th>
      <th>Скачать</th>
      <th>Удалить</th>
    </tr>


    <?php foreach($events_all_data as $event) : ?>
      <tr>
        <td><?= $event['id']; ?></td>
        <td><?= $event['symbol_id']; ?></td>
        <td><?= $event['title']; ?></td>
        <td><a href="editEvent.php?edit=<?= $event['id']; ?>">Редактировать</a></td>
        <td><a href="?csv=<?= $event['symbol_id']; ?>">CSV</a></td>
        <td class="js-reload">
          <a href="?del=<?= $event['id']; ?>&name=<?= $event['symbol_id']; ?>">X</a>
        </td>
      </tr>
    <?php endforeach; ?>

  </table>

</div>
