<div class="container container_1600">

  <a class='btn' href='index.php?logout'>Выйти</a>
  <a class='btn' href='events.php'>События</a>

  <h1 class="title center mb2em">Зарегистрированные участники</h1>

  <form class='flex-between m20' action="index.php" method="POST">
    <div>
      <span>для выбора магазина(таблицы)</span>
      <select name="tableName">

        <?php foreach ($events_titles as $table => $value) : ?>
          <option <?= $tableName == $table ?  'selected' : ''; ?> value="<?= $table; ?>"><?= $value; ?></option>
        <?php endforeach; ?>

      </select>
      <input type="submit" value="Показать">
    </div>
    <span class="priority">Количество участников: <strong><?= count($members) ?></strong></span>
  </form>

  <table class="simple-little-table" border="1" cellpadding="5" cellspacing="0" width="100%">
    <tr>
      <th>ID</th>
      <th>Имя</th>
      <th>Фамилия</th>
      <th>Почта</th>
      <th>Телефон</th>
      <th>Время</th>
    </tr>
    <?php

    if (!count($members)) : ?>
      <tr>
        <td>Нет записей!</td>
      </tr>
    <?php
    else :
      foreach($members as $item) : ?>
        <tr>
          <td><?= $item['id']; ?></td>
          <td><?= $item['name']; ?></td>
          <td><?= $item['surname']; ?></td>
          <td><?= $item['email']; ?></td>
          <td><?= $item['phone']; ?></td>
          <td><?= $item['time']; ?></td>
        </tr>
      <?php
      endforeach;
    endif; ?>

  </table>

</div>
