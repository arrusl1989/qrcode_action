<!DOCTYPE HTML>
<html lang="ru">
  <head>
    <title><?= $title; ?></title>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="../img/favicon/favicon-16x16.png" type="image/x-icon">
    <link href="https://fonts.googleapis.com/css?family=Roboto&amp;subset=cyrillic" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css">
  </head>
  <body>
