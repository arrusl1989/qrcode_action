<!DOCTYPE HTML>
<html>
  <head>
    <title>Создание пользователя</title>
    <meta charset="utf-8">
    <link rel="shortcut icon" href="../img/favicon/favicon-16x16.png" type="image/x-icon">
    <link rel="stylesheet" href="../css/style.css">
  </head>

  <body>

    <div class="container">
      <div class="center">
        <h1 class="title">Создание пользователя</h1>
        <h3 class="title"><?= $result; ?></h3>
        <form class="form" action="<?= $_SERVER['PHP_SELF']; ?>" method="post">

          <label for="txtUser">Логин</label>
          <input id="txtUser" type="text" name="login" value="<?= $login; ?>"/>

          <label for="txtString">Пароль</label>
          <input id="txtString" type="text" name="password" value="<?= $password; ?>"/>

          <input type="submit" value="Создать">
        </form>
        <div class="txt_center">
          <a href="../events.php" class="btn">Назад</a>
        </div>
      </div>
    </div>

  </body>
</html>

