(function (){
    
    var btn = document.querySelectorAll('.js-reload');

    for (var i = 0; i < btn.length; i++) {
        var element = btn[i];
        
        element.addEventListener('click', function() {
            setTimeout(function() {
                location.reload();
            }, 1000);
        });
    }
})();