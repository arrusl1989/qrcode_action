<?php
    require_once __DIR__ . "/secure/session.inc.php";
    require_once __DIR__ . "/secure/secure.inc.php";

    if (isset($_GET['logout'])) {
        logOut();
    }

    $title = 'Участники';

    $events_all_data = getAllDataFromTable($connection, '_events');

    $events_titles = getDesiredVal($events_all_data, 'symbol_id', 'title');

    // для выбора магазина(таблицы)
    !$_POST['tableName']
        ? $tableName = $events_all_data[0]['symbol_id']
        :  $tableName = $_POST['tableName'];

   $members = getAllDataFromTable($connection, $tableName);

    if ($members === false) {
        echo "Error";
        exit;
    }

    include getView('v-header', 'admin');
    include getView('v-index', 'admin');
    include getView('v-footer', 'admin');
