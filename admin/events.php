<?php
  require_once __DIR__ . "/secure/session.inc.php";
  require_once __DIR__ . "/secure/secure.inc.php";

    $title = 'События';
    $events_all_data = getAllDataFromTable($connection, '_events');

    // скачать csv
    if ($_GET['csv']) {

      $symbol_id = clearStr($connection, $_GET['csv']);

      $table = getDataForCsv($connection, $symbol_id);

      if (createCsvFile($table, 'exports/' . $symbol_id . '.csv')) {
          header('Location: exports/' . $symbol_id . '.csv');
          exit;
      } else {
          echo 'Ошибка при создании файла csv';
      }
      // удалить событие
    } else if ($_GET['del']) {
      $symbol_id = $_GET['name'] ? clearStr($connection, $_GET['name']) : null;
      $del   = $_GET['del'] ? clearInt($_GET['del']) : null;

      $table = getDataForCsv($connection, $symbol_id);
        createCsvFile($table, 'exports/' . $symbol_id . '.csv');

       if (delEvent($connection, $del) && dropTable($connection, $symbol_id)) {
            header('Location: exports/' . $symbol_id . '.csv');
            exit;
        } else {
            echo 'Ошибка при удалении события или удалении таблицы';
        }
    }

    include getView('v-header', 'admin');
    include getView('v-events', 'admin');
    include getView('v-footer', 'admin');
