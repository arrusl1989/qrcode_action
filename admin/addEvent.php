<?php
  require_once __DIR__ . "/secure/session.inc.php";
  require_once __DIR__ . "/secure/secure.inc.php";

  $title = 'Добавить событие';

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $symbol_id = clearStr($connection, $_POST['symbol_id']);
    $name = clearStr($connection, $_POST['name']);

    if (addEvent($connection, $symbol_id, $name)
        && createTable($connection, $symbol_id)) {
        header('Location: events.php');
        exit;
    } else {
        echo "Ошибка";
    }

  }

  include getView('v-header', 'admin');
  include getView('v-addEvent', 'admin');
  include getView('v-footer', 'admin');
