<?php
$title = 'Авторизация';
$login  = '';

session_start();
 header("HTTP/1.0 401 Unauthorized");

require_once "secure.inc.php";
require __DIR__ . '/../../inc/config.php';

if ($_SERVER['REQUEST_METHOD'] == 'POST') {

	$login = clearStr($connection, $_POST["login"]);
	$pw    =  clearStr($connection, $_POST["pw"]);
	$ref   =  clearStr($connection, $_GET["ref"]);

	if (!$ref)
		$ref = '../index.php';

	if ($login and $pw) {
		if ($result = userExists($login)) {
			list($_, $hash) = explode(':', $result);
			if (checkHash($pw, $hash)) {
				$_SESSION['admin'] = true;
				header("Location: $ref");
				exit;
			} else {
				$title = 'Неправильное имя пользователя или пароль!';
			}
		} else {
			$title = 'Неправильное имя пользователя или пароль!';
		}
	} else {
		$title = 'Заполните все поля формы!';
	}

}
include getView('v-login', 'admin');

