<?php

const FILE_NAME = '.htpasswd';

/**
 * Получение хеша пароля
 * @param  string $password
 * @return string
 */
function getHash($password){
    $hash = password_hash($password, PASSWORD_BCRYPT);
    return $hash;
}

/**
 * Проверка на соответствие пароля хешу
 * @param  string $password
 * @param  string $hash
 * @return bool
 */
function checkHash($password, $hash){
    return password_verify(trim($password), trim($hash));
}

/**
 * Сохранение пользователя
 * @param  string $login
 * @param  string $hash
 * @return bool
 */
function saveUser($login, $hash){
    $str = "$login:$hash\n";
    if (file_put_contents(FILE_NAME, $str, FILE_APPEND)) {
        return true;
    }
        return false;
}

/**
 * Проверяем пользователя на существование
 * @param  string $login
 * @return bool
 */
function userExists($login){
    if (!is_file(FILE_NAME))
        return false;

    $users = file(FILE_NAME);

    foreach ($users as $user) {
      if (strpos($user, $login.':') !== false)
          return $user;
    }

    return false;
}

/**
 * Выход из админки
 */
function logOut(){
    session_destroy();
    header('Location: secure/login.php');
    exit;
}
