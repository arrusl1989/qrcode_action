<?php
  require_once __DIR__ . "/session.inc.php";
  require_once __DIR__ . "/secure.inc.php";

  $login = 'root';
  $password = '1234';
  $result = '';

  if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    $login = $_POST['login'] ?: $login;
    
    if(!userExists($login)) {
      $password = $_POST['password'] ?: $password;
      $hash = getHash($password);

      if (saveUser($login, $hash)) {
        $result = 'Пользователь '.$login.' создан!';
      } else {
        $result = 'При записи хеша '. $hash. ' произошла ошибка';
      }

    } else {
      $result = "Пользователь $login уже существует. Выберите другое имя.";
    }
  }

  include getView('v-createUser','admin' );

