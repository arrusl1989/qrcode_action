<?php
    require_once __DIR__ . "/secure/session.inc.php";
    require_once __DIR__ . "/secure/secure.inc.php";
  
    $id    = $_GET['edit'] ? clearStr($connection, $_GET['edit']) : null;
    $event = getEventById($connection, $id)[0];
    $title = 'Редактирование события';

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        $id = clearInt($_POST['id']);
        $name = clearStr($connection, $_POST['name']);

        if (updateEvent($connection, $id, $name)) {
            header('Location: events.php');
            exit;
        } else {
            echo "Ошибка при обновлении события";
        }

    }

    include getView('v-header', 'admin');
    include getView('v-editEvent', 'admin');
    include getView('v-footer', 'admin');

    


