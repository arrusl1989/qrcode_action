$(function() {

    $('body').on('submit', 'form', function (evt) {
        var $this = $(this);

        if (!isFormValidate($this)) {
            evt.preventDefault();
            evt.stopPropagation();
            $('.has-error', $this).first().focus();

        } else if ($this.hasClass('js-ajax-form')) {
            evt.preventDefault();
            $this.addClass('load');

            sendFormAjax($this, function (data) {
                formSendResult(data);
                $this.removeClass('load');
            });
        }
    });

    $('body').on('change', '.has-error', function () {
        var $this = $(this);

        if (isFieldValidate($this)) {
            $this.closest('.has-error').removeClass('has-error');
        }
    });

    $('body').on('click', '.js-repeat', function () {
        var form = $('.form');

        isFormValidate(form) ?
                form.submit() :
                    $('#fail').fadeOut();
        
    });

    /**
     * Валидация E-mail адреса
     * @param {string} emailAddress - e-mail для проверки
     * @returns {Boolean}
     */
    function isValidEmail(emailAddress) {
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        return pattern.test(emailAddress);
    }

    /**
     * Валидация всей формы
     * @param {object} form jQuery объект формы
     * @param {string} error_class класс ошибки
     * @returns {Boolean}
     */
    function isFormValidate(form, error_class) {
        var result = true,
                rq = $('.required', form).length,
                check = [
                    'input[type="text"]',
                    'input[type="login"]',
                    'input[type="password"]',
                    'input[type="number"]',
                    'input[type="checkbox"]',
                    'input[type="tel"]',
                    'input[type="email"]',
                    'input[type="textarea"]',
                    'input[type="select"]',
                    'textarea',
                    'select[name="salon"]',
                    'select',
                ],
                parent;

        error_class = error_class || 'has-error';

        $('.required, input, textarea, select').removeClass(error_class);

        if (rq < 1) {
            return result;
        }

        for (var i = 0; i < rq; i++) {
            parent = $('.required', form).eq(i);

            $(check.join(','), parent).each(function () {
                if (!isFieldValidate($(this), error_class)) {
                    return result = false;
                }
            });
        }

        return result;
    }

    /**
     * Проверка валидации поля
     * @param {object} field jQuery объект поля формы
     * @param {string} error_class класс ошибки
     * @returns {Boolean}
     */
    function isFieldValidate(field, error_class) {
        var result = true;
        if (field.val() == null) {
            field.val('false');
        }

        if (notNull(field) && notNull(field.attr('name')) && field.attr('name') !== '') {
            var val = (field.val() + '').trim();

            if (field.hasClass('valid_email') && !isValidEmail(val)) {
                result = false;
            } else if (field.attr('type') === 'checkbox' && !field.is(':checked')) {
                result = false;
            } else if (!notNull(val) || val === '' || val === field.data('mask')) {

                result = false;
            }
        }


        if (!result) {
            field.addClass(error_class);
        } else {
            field.removeClass(error_class);
        }

        return result;
    }

    /**
     * Проверяем значение на null и undefined
     * @param {mixed} val значение
     * @returns {Boolean}
     */
    function notNull(val) {
        return val !== null && val !== undefined;
    }

    /**
     * Отправляем форму ajax
     * @param {object} form jQuery объект формы
     * @param {function} callback функция обратного вызова
     */
    function sendFormAjax(form, callback) {
        sendAjax(form.attr('action') || '/', form.serialize(), callback);
    }

    /**
     * Отправляем ajax запрос
     * @param {string} url ссылка
     * @param {object} data данные
     * @param {function} callback функция обратного вызова
     */
    function sendAjax(url, data, callback) {
        callback = callback || function () {
        };

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: data,
            success: function (data) {
                callback(data);
            },
            error: function () { 
                $('#fail').fadeIn();
                if ($('.form').hasClass('load')) {
                    $('.form').removeClass('load');
                }
            }
        });
    }

    /**
     * Обработка отправки формы
     * @param {object} data данные полученные от сервера
     */
    function formSendResult(data) {
        if (data.status === 'done') {
            $('#done').fadeIn();
        }
    }

});