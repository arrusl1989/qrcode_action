$(function() {

    $('body').on('keydown', 'input[name="phone"]', function (e) {
        var t = $(this);

        if ((t.val() === t.data('mask') || t.val() === '') && e.key === '8') {
            e.preventDefault();
        }
    });

    $('body').on('focus', 'input[name="phone"]', function (e) {
        $(this).mask('+7 (999) 999-99-99').data('mask', '+7 (___) ___-__-__');
    });

    $('body').on('click', 'input[name="phone"]', function (e) {
        var t = $(this);
        if ((t.val() === t.data('mask') || t.val() === '')) {
            t.focus()
        }
    });

});